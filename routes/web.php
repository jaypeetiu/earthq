<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

Route::redirect('/', '/login');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home')->middleware('auth');
Route::get('/news', 'NewsController@index')->name('news')->middleware('auth');
Route::get('/tips', 'TipsController@index')->name('tips')->middleware('auth');
Route::get('/intensityscale', 'IntensityScaleController@index')->name('intensityscale')->middleware('auth');
Route::get('/earthquakeinfo', 'EarthquakeInfoController@index')->name('earthquakeinfo')->middleware('auth');
Route::get('/tsunamiinfo', 'TsunamiInfoController@index')->name('tsunamiinfo')->middleware('auth');


// Route::group(['middleware' => 'auth'], function () {
// 		Route::get('icons', ['as' => 'pages.icons', 'uses' => 'PageController@icons']);
// 		Route::get('maps', ['as' => 'pages.maps', 'uses' => 'PageController@maps']);
// 		Route::get('notifications', ['as' => 'pages.notifications', 'uses' => 'PageController@notifications']);
// 		Route::get('rtl', ['as' => 'pages.rtl', 'uses' => 'PageController@rtl']);
// 		Route::get('tables', ['as' => 'pages.tables', 'uses' => 'PageController@tables']);
// 		Route::get('typography', ['as' => 'pages.typography', 'uses' => 'PageController@typography']);
// 		Route::get('upgrade', ['as' => 'pages.upgrade', 'uses' => 'PageController@upgrade']);
// });

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

	Route::get('/push-notificaiton', 'WebNotificationController@index')->name('push-notificaiton');
	Route::get('/news', 'NewsController@index')->name('news');
	Route::post('/store-token', 'WebNotificationController@storeToken')->name('store.token');
	Route::post('/send-web-notification', 'WebNotificationController@sendWebNotification')->name('send.web-notification');

	Route::resource('intent', 'IntentController', ['except' => ['show']]);
	// Route::post('/devicetoken', 'UserController@storeToken');
	Route::get('/send-alert/{id}', 'IntentController@sendAlert')->name('sendAlert');
});

// Route::post('/devicetoken', 'UserController@storeToken');

