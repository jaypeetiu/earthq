@extends('layouts.app', ['page' => __('News'), 'pageSlug' => 'news'])

@section('content')
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h1 class="title">{{ __('Recent News') }}</h1>
                    <span>10/27/2022</span>
                </div>
            </div>

            <div class="card">
                <div>
                    <a href="#">
                        <img class="news" src="black/img/bg5.jpg" alt="">
                    </a>
                    <div class="card-header">
                        <h4 class="title">
                        {{ __('Magnitude 5.6 quake rocks parts of Mindanao') }}
                        </h4>
                    </div>
                    <div class="description">
                        <p class="p-3">
                            Aug 15 - A Magnitude 5.6 earthquake jolted parts of Mindanao Monday afternoon, the Philippine Institute of Volcanology and Seismology (Phivolcs) said.
                            The epicenter of the quake, which is of tectonic origin, was recorded 11 kilometers northwest of Magsaysay, Davao del Sur, Phivolcs said.
                        </p>
                        <p class="p-3">
                            It struck at 4:23 p.m. at a depth of six kilometers.
                        </p>
                        <p class="p-3">source: https://www.mindanews.com/top-stories/2022/08/magnitude-5-6-quake-rocks-parts-of-mindanao/</p>
                    </div>

                    <div>

                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card card-user">
                <div class="card-body">
                    <p class="card-text">
                        <div class="">
                            <a href="#">
                                <img class="news" src="black/img/bg5.jpg" alt="">
                                <h4 class="description" style="text-decoration: underline;">
                                    8 dead, 14 missing in a hospital after a Magnitude 6.3 hits General Santos City
                                </h4>
                            </a>
                        </div>
                    </p>
                </div>
                <div class="card-body">
                    <p class="card-text">
                        <div class="">
                            <a href="#">
                                <img class="news" src="black/img/header.jpg" alt="">
                                <h4 class="description" style="text-decoration: underline;">
                                    167 Aftershocks recorded in 1 hour after big earthquake hits Davao
                                </h4>
                            </a>
                        </div>
                    </p>
                </div>
                <div class="card-body">
                    <p class="card-text">
                        <div class="">
                            <a href="#">
                                <img class="news" src="black/img/img_3115.jpg" alt="">
                                <h4 class="description" style="text-decoration: underline;">
                                    Magnitude 5.9 earthquake hits Maguindanao
                                </h4>
                            </a>
                        </div>
                    </p>
                </div>
            </div>
        </div>
    </div>
@endsection
