@extends('layouts.app', ['page' => __('INFO & RESOURCES'), 'pageSlug' => 'earthquakeinfo'])

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h1 class="title">What is an Earthquake?</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-body">
                    <p class="text-muted" style="padding: 4px;">
                        An earthquake is a weak to strong shaking of the ground due to sudden movement or displacement of the rocks underneat. <br />
                    </p>
                    <p class="text-muted" style="padding: 4px;">
                        The earthquakes originate in tectonic plate boundary. The focus is point inside the earth where the earthquake started, sometimes called the hypocenter, and the point on the surface of the earth directly above the focus is called the epicenter.
                    </p>

                    <img class="news" src="black/img/earthquake_info_1.png" alt="">

                    <h4 class="card-title" style="margin-left: 6px;margin-top: 30px;">What are the possible effects of an earthquake?</h4>
                    <p class="text-muted" style="padding: 4px;">
                        Strong ground shaking can cause injuries to people due to broken glasses and fallen objects. Buildings maybe damaged. building s that are not properly constructed can collapse and can cause death.                
                    </p>
                    <h4 class="card-title" style="margin-left: 6px;margin-top: 30px;">Types of earthquake</h4>
                    <p class="text-muted" style="padding: 4px;">
                        There are two types of earthquakes: tectonic and volcanic earthquakes. <b>Tectonic</b> Earthquakes are produced by sudden movement along faults and plate boundaries. Earthquakes induced by rising lava or magma beneath active volcanoes is called <b>Volcanic</b> Earthquakes.                
                    </p>                    
                    <h4 class="card-title" style="margin-left: 6px;margin-top: 30px;">Two ways to measure the strength of an Earthquake</h4>
                    <p class="text-muted" style="padding: 4px;">
                        There are two ways by which we can measure the strength of an earthquake: magnitude and intensity. Magnitude is proportional to the energy released by an earthquake at the focus. It is calculated from earthquakes recorded by an instrument called seismograph. It is represented by Arabic Numbers (e.g. 4.8, 9.0). Intensity on the other hand, is the strength of an earthquake as perceived and felt by people in a certain locality. It is a numerical rating based on the relative effects to people, objects, environment, and structures in the surrounding. The intensity is generally higher near the epicenter. It is represented by Roman Numerals (e.g. II, IV, IX). In the Philippines, the intensity of an earthquake is determined using the PHIVOLCS Earthquake Intensity Scale (PEIS).                  
                    </p>
                </div>
            </div>
        </div>
    </div>
    
@endsection
