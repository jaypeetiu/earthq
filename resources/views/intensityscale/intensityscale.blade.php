@extends('layouts.app', ['page' => __('INFO & RESOURCES'), 'pageSlug' => 'intensityscale'])

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h1 class="title">Earthquake Intensity Scale</h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-12">
            <div class="card card-chart">
                <div class="card-header ">
                    <div class="row">
                        <div class="col-sm-6 text-left">
                            <h5 class="card-category">PHIVOLCS</h5>
                            <h2 class="card-title">Earthquake Intensity Scale</h2>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <p class="text-muted" style="padding: 4px;">A measure of how an earthquake was felt in ascertain locality or area. It is based on relative effect to people, structures, and objects in the surroundings. It is represented by Roman Numerals, with intensity I being the weakest and intensity X the strongest. It is used since 1996, replacing the Rossi-Forel scale.</p>
                    <h4 class="card-title" style="margin-left: 6px;margin-top: 30px;">I. SCARCELY PERCEPTIBLE</h4>
                    <p class="text-muted" style="padding: 4px;">
                        - Perceptible to people only under favorable circumstances. <br />
                        - delicately -balanced objects are distributed slightly. <br />
                        - Still water in containers oscillated slightly. <br />                    
                    </p>
                    <h4 class="card-title" style="margin-left: 6px;margin-top: 30px;">II. SLIGHTLY FELT</h4>
                    <p class="text-muted" style="padding: 4px;">
                        - Felt by few individuals at rest indoors. <br />
                        - Hanging objects swing slightly. <br />
                        - Still water in containers oscillates noticeably. <br />                    
                    </p>
                    <h4 class="card-title" style="margin-left: 6px;margin-top: 30px;">III. WEAK</h4>
                    <p class="text-muted" style="padding: 4px;">
                        - Felt by many people indoors specially in upper floors of buildings. Vibration is felt like the passing of a light truck. Dizziness and nausea are experienced by some people. <br />
                        - Hanging objects swing moderately. <br />
                        - Still water in containers oscillates moderately. <br />                    
                    </p>
                    <h4 class="card-title" style="margin-left: 6px;margin-top: 30px;">IV. MODERATELY STRONG</h4>
                    <p class="text-muted" style="padding: 4px;">
                        - Felt generally by people indoors and some people outdoors. Light sleepers are awakened. Vibration is felt like passing of a heavy truck. <br />
                        - Hanging objects swing considerably. Dinner plates, glasses, windows and doors rattle. Floors and walls of wood-framed buildings creak. Standing motor cars may rock slightly. <br />
                        - Water in containers oscillates strongly. <br />   
                        - Rumbling sounds may sometimes be heard. <br />                 
                    </p>
                    <h4 class="card-title" style="margin-left: 6px;margin-top: 30px;">V. STRONG</h4>
                    <p class="text-muted" style="padding: 4px;">
                        - Generally felt by most people indoors and outdoors. Many sleeping people awakened. Some are frightened, some run outdoors. Strong shaking and rocking are felt throughout the building. <br />                
                    </p>

                    <img class="news" src="black/img/earthquake_info_3.png" alt="">
                </div>
            </div>
        </div>
    </div>
    
@endsection
