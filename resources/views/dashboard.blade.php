@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
<div class="row">
    <div class="col-12">
    @include('alerts.success')
        <div class="card text-center align-middle">
            <div class="card-header">
                <h5 class="card-category">Intensities (Roman Numeral)</h5>
            </div>
            <div class="card-body">
                <div class="alert-button" style="margin: 60px;">
                    <button type="button" class="btn btn-primary" style="font-size: 50px;width: 200px;" onclick="return confirm('Are you sure you want to proceed?') ? window.location.href='/send-alert/2' : ''">II</button>
                    <button type="button" class="btn btn-secondary" style="font-size: 50px;width: 200px;" onclick="return confirm('Are you sure you want to proceed?') ? window.location.href='/send-alert/3' : ''">III</button>
                    <button type="button" class="btn btn-success" style="font-size: 50px;width: 200px;" onclick="return confirm('Are you sure you want to proceed?') ? window.location.href='/send-alert/4' : ''">IV</button>
                    <button type="button" class="btn btn-danger" style="font-size: 50px;width: 200px;" onclick="return confirm('Are you sure you want to proceed?') ? window.location.href='/send-alert/5' : ''">V</button>
                    <button type="button" class="btn btn-warning" style="font-size: 50px;width: 200px;" onclick="return confirm('Are you sure you want to proceed?') ? window.location.href='/send-alert/6' : ''">VI</button>
                    <button type="button" class="btn btn-info" style="font-size: 50px;width: 200px;" onclick="return confirm('Are you sure you want to proceed?') ? window.location.href='/send-alert/7' : ''">VII</button>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@push('js')
<!-- <script src="black/js/plugins/chartjs.min.js"></script> -->
<script>
    $(document).ready(function() {
        if(window.location.href == `{{env('APP_URL')}}/home`){
            window.location.href = `{{env('APP_URL')}}/home?user={{$user->name}}&id={{$user->id}}`
        }
    });
</script>
@endpush