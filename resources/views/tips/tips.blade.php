@extends('layouts.app', ['page' => __('INFO & RESOURCES'), 'pageSlug' => 'tips'])

@section('content')
    <div class="row">
        <div class="card">
            <div class="card-header">
                <h1 class="title">Earthquakes: Do's & Don'ts</h1>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div>
                    <a href="#">
                        <img class="news" src="black/img/cover_4.jpg" alt="">
                    </a>
                    <div class="card-header">
                        <h4 class="title">
                        {{ __('What to Do Before an Earthquake') }}
                        </h4>
                    </div>
                    <div class="description">
                        <li class="p-3">
                            Store breakable items such as bottled foods, glass, and china in low, closed cabinets with latches.
                        </li>
                        <li class="p-3">
                            Place large or heavy objects on lower shelves.
                        </li>
                        <li class="p-3">
                            Secure water heaters, LPG cylinders etc., by strapping them to the walls or bolting to the floor.
                        </li>
                        <li class="p-3">
                        Know emergency telephone numbers (such as those of doctors, hospitals, the police, etc)
                        </li>
                    </div>

                    <div>

                    </div>
                </div>
            </div>
            
        </div>

        <div class="col-md-4">
            <div class="card">
                    <div>
                        <a href="#">
                            <img class="news" src="black/img/cover_3.jpg" alt="">
                        </a>
                        <div class="card-header">
                            <h4 class="title">
                            {{ __('Have a disaster emergency kit ready') }}
                            </h4>
                        </div>
                        <div class="description">
                            <li class="p-3">
                                Battery operated torch with extra batteries
                            </li>
                            <li class="p-3">
                                Battery operated radio
                            </li>
                            <li class="p-3">
                                First aid kit and manual
                            </li>
                            <li class="p-3">
                                Emergency food (dry items) and water (packed and sealed)
                            </li>
                            <li class="p-3">
                                Candles and matches in a waterproof container
                            </li>
                        </div>

                        <div>

                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                    <div>
                        <a href="#">
                            <img class="news" src="black/img/cover_2.jpg" alt="">
                        </a>
                        <div class="card-header">
                            <h4 class="title">
                            {{ __('Develop an emergency communication plan') }}
                            </h4>
                        </div>
                        <div class="description">
                            <li class="p-3">
                                In case family members are separated from one another during an earthquake (a real possibility during 
                                the day when adults are at work and children are at school), develop a plan for reuniting after the 
                                disaster.
                            </li>
                            <li class="p-3">
                                Ask an out-of-state relative or friend to serve as the 'family contact' after the disaster; it is often 
                                easier to call long distance. Make sure everyone in the family knows the name, address, and phone number 
                                of the contact person.
                            </li>
                        </div>

                        <div>

                        </div>
                    </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                    <div>
                        <a href="#">
                            <img class="news" src="black/img/cover_4.jpg" alt="">
                        </a>
                        <div class="card-header">
                            <h4 class="title">
                            {{ __('Help your community get ready') }}
                            </h4>
                        </div>
                        <div class="description">
                            <li class="p-3">
                                Publish a special section in your local newspaper with emergency information on earthquakes. Localize the information 
                                by printing the phone numbers of local emergency services offices and hospitals.
                            </li>
                            <li class="p-3">
                                Conduct week-long series on locating hazards in the home.
                            </li>
                            <li class="p-3">
                                Work with local emergency services and officials to prepare special reports for people with mobility impairment on what to do during an earthquake.
                                Provide tips on conducting earthquake drills in the home.
                            </li>
                        </div>

                        <div>

                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                    <div>
                        <a href="#">
                            <img class="news" src="black/img/cover_1.jpg" alt="">
                        </a>
                        <div class="card-header">
                            <h4 class="title">
                            {{ __('What to Do During an Earthquake (Indoor)') }}
                            </h4>
                        </div>
                        <div class="description">
                            <li class="p-3">
                                DROP to the ground; take COVER by getting under a sturdy table or other piece of furniture; and HOLD ON until the shaking stops. If there is no a 
                                table or desk near you, cover your face and head with your arms and crouch in an inside corner of the building.
                            <li class="p-3">
                                Stay in bed if you are there when the earthquake strikes. Hold on and protect your head with a pillow, unless you are under a heavy light fixture that could fall. 
                                In that case, move to the nearest safe place.
                            </li>
                        </div>

                        <div>

                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                    <div>
                        <a href="#">
                            <img class="news" src="black/img/cover_5.jpg" alt="">
                        </a>
                        <div class="card-header">
                            <h4 class="title">
                            {{ __('What to Do During an Earthquake (Outdoor)') }}
                            </h4>
                        </div>
                        <div class="description">
                            <li class="p-3">
                                Do not move from where you are. However, move away from buildings, trees, streetlights, and utility wires.
                            <li class="p-3">
                                If you are in open space, stay there until the shaking stops. The greatest danger exists directly outside buildings; at exits; and alongside exterior walls. Most earthquake-related 
                                casualties result from collapsing walls, flying glass, and falling objects.
                            </li>
                        </div>

                        <div>

                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="card">
                    <div>
                        <a href="#">
                            <img class="news" src="black/img/cover_3.jpg" alt="">
                        </a>
                        <div class="card-header">
                            <h4 class="title">
                            {{ __('What to Do During an Earthquake (If in a moving vehicle)') }}
                            </h4>
                        </div>
                        <div class="description">
                            <li class="p-3">
                                Stop as quickly as safety permits and stay in the vehicle. Avoid stopping near or under buildings, trees, overpasses, and utility wires.
                            <li class="p-3">
                                Proceed cautiously once the earthquake has stopped. Avoid roads, bridges, or ramps that might have been damaged by the earthquake.
                            </li>
                        </div>

                        <div>

                        </div>
                    </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                    <div>
                        <a href="#">
                            <img class="news" src="black/img/cover_5.jpg" alt="">
                        </a>
                        <div class="card-header">
                            <h4 class="title">
                            {{ __('What to Do During an Earthquake (If trapped under debris)') }}
                            </h4>
                        </div>
                        <div class="description">
                            <li class="p-3">
                                Do not light a match.
                            <li class="p-3">
                                Cover your mouth with a handkerchief or clothing.
                            </li>
                            <li class="p-3">
                                Tap on a pipe or wall so rescuers can locate you. Use a whistle if one is available. Shout only as a last resort. Shouting can cause you to inhale dangerous amounts of dust.
                            </li>
                        </div>

                        <div>

                        </div>
                    </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="card">
            <div class="card-header">
                <h1 class="title">Safety Tips</h1>
            </div>
        </div>

        <div class="col-md-8">
            <div>
                <iframe width="357" height="315"
                    src="https://www.youtube.com/embed/5Na5GOIgpq8">
                </iframe>
            </div>
        </div>
    </div>

    
@endsection
