@extends('layouts.app', ['pageSlug' => 'dashboard'])

@section('content')
<div class="row">
    <div class="col-12">
        <div class="card card-chart">
            <div class="card-header ">
                <div class="row">
                    <div class="col-sm-6 text-left">
                        <h5 class="card-category">Welcome Back,</h5>
                        <h2 class="card-title">{{ auth()->user()->name }}</h2>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <p class="text-muted" style="padding: 4px;">Shake CDO App is an earthquake warning app that sends an earthquake advisory on time. In partnership with the LGU, CDRRMC, and Phivolcs X.</p>
                <p class="text-muted" style="padding: 4px;">This mobile app enables you to be alert in times of Earthquake that might occur in the City of Cagayan de Oro. This app has many features that you can use, and the main function of this is to notify your mobile phone whenever an Earthquake hits the city.</p>
                <p class="text-muted" style="padding: 4px;">Residents of Cagayan de Oro City will be notified during an Earthquake and will keep you alerted in the occurrence of calamity.</p>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-12">
        <div class="card ">
            <div class="card-header">
                <h4 class="card-title">Previous Data (Intensity)</h4>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table tablesorter" id="">
                        <thead class=" text-primary">
                            <tr>
                                <th>
                                    Date
                                </th>
                                <th>
                                    Depth
                                </th>
                                <th>
                                    Magnitude
                                </th>
                                <th class="text-center">
                                    Epicenter
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($intents as $intent)
                            <tr>
                                <td>
                                    {{$intent->date}}
                                </td>
                                <td>
                                    {{$intent->depth}}
                                </td>
                                <td>
                                    {{$intent->magnitude}}
                                </td>
                                <td class="text-center">
                                    {{$intent->name}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4">
        <div class="card card-chart">
            <div class="card-header">
                <h5 class="card-category">NEWS</h5>
                <h3 class="card-title"><i class="tim-icons icon-bell-55 text-primary"></i>See Current News and Latest Headlines.</h3>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <a href="/news" type="button" class="btn btn-primary">Go to News</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-chart">
            <div class="card-header">
                <h5 class="card-category">TIPS</h5>
                <h3 class="card-title"><i class="tim-icons icon-delivery-fast text-info"></i>Know the basics during and after an earthquake.</h3>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <a href="/tips" type="button" class="btn btn-primary">View Do's and Donts</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card card-chart">
            <div class="card-header">
                <h5 class="card-category">LEARN</h5>
                <h3 class="card-title"><i class="tim-icons icon-send text-success"></i>Learn what is a tsunami.</h3>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <a href="/tsunamiinfo" type="button" class="btn btn-primary">Learn about Tsunami's</a>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection

@push('js')
<script src="black/js/plugins/chartjs.min.js"></script>
<script>
    $(document).ready(function() {
        if (window.location.href == `{{env('APP_URL')}}/home`) {
            window.location.href = `{{env('APP_URL')}}/home?user={{$user->name}}&id={{$user->id}}`
        }
        demo.initDashboardPageCharts();
    });
</script>
@endpush