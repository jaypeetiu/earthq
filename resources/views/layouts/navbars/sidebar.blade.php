<div class="sidebar">
    <div class="sidebar-wrapper">
        <div class="logo">
            <a href="/" class="simple-text logo-mini">{{ __('SC') }}</a>
            <a href="/" class="simple-text logo-normal">{{ __('ShakeCDO') }}</a>
        </div>
        <ul class="nav">
            @can('user_access')
            <li @if ($pageSlug=='dashboard' ) class="active " @endif>
                <a href="{{ route('home') }}">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            @endcan
            @can('admin_access')
            <li @if ($pageSlug=='dashboard' ) class="active " @endif>
                <a href="{{ route('home') }}">
                    <i class="tim-icons icon-chart-pie-36"></i>
                    <p>{{ __('Dashboard') }}</p>
                </a>
            </li>
            @endcan
            @can('admin_access')
            <li @if ($pageSlug=='intent' ) class="active " @endif>
                <a href="{{ route('intent.index')  }}">
                    <i class="tim-icons icon-world"></i>
                    <p>{{ __('Intentsity Data') }}</p>
                </a>
            </li>
            @endcan
            @can('user_access')
                <li @if ($pageSlug=='news' ) class="active " @endif>
                    <a href="{{ route('news')  }}">
                        <i class="tim-icons icon-book-bookmark"></i>
                            <p>{{ __('News') }}</p>
                    </a>
                </li>

                <li>
                    <a data-toggle="collapse" href="#info-management" aria-expanded="true">
                        <i class="tim-icons icon-bulb-63"></i>
                        <span class="nav-link-text">{{ __('INFO & RESOURCES') }}</span>
                        <b class="caret mt-1"></b>
                    </a>

                    <div class="collapse show" id="info-management">
                        <ul class="nav pl-4">
                            <li @if ($pageSlug=='tips' ) class="active "  @endif>
                                <a href="{{ route('tips')  }}">
                                    <i class="tim-icons icon-time-alarm"></i>
                                        <p>{{ __('Tips') }}</p>
                                </a>
                            </li>
                            <li @if ($pageSlug=='intensityscale' ) class="active "  @endif>
                                <a href="{{ route('intensityscale')  }}">
                                    <i class="tim-icons icon-sound-wave"></i>
                                        <p>{{ __('Intensity Scale') }}</p>
                                </a>
                            </li>
                            <li @if ($pageSlug=='earthquakeinfo' ) class="active "  @endif>
                                <a href="{{ route('earthquakeinfo')  }}">
                                    <i class="tim-icons icon-map-big"></i>
                                        <p>{{ __('Earthquake') }}</p>
                                </a>
                            </li>
                            <li @if ($pageSlug=='tsunamiinfo' ) class="active "  @endif>
                                <a href="{{ route('tsunamiinfo')  }}">
                                    <i class="tim-icons icon-user-run"></i>
                                        <p>{{ __('Tsunami') }}</p>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            @endcan
            <li>
                <a data-toggle="collapse" href="#user-management" aria-expanded="true">
                    <i class="tim-icons icon-bullet-list-67"></i>
                    <span class="nav-link-text">{{ __('User Management') }}</span>
                    <b class="caret mt-1"></b>
                </a>

                <div class="collapse show" id="user-management">
                    <ul class="nav pl-4">
                        <li @if ($pageSlug=='profile' ) class="active " @endif>
                            <a href="{{ route('profile.edit')  }}">
                                <i class="tim-icons icon-single-02"></i>
                                <p>{{ __('User Profile') }}</p>
                            </a>
                        </li>
                        @can('admin_access')
                        <li @if ($pageSlug=='users' ) class="active " @endif>
                            <a href="{{ route('user.index')  }}">
                                <i class="tim-icons icon-bullet-list-67"></i>
                                <p>{{ __('User Lists') }}</p>
                            </a>
                        </li>
                        @endcan
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>