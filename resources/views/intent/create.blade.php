@extends('layouts.app', ['page' => __('Intensity Alert'), 'pageSlug' => 'intent'])

@section('content')
<div class="container-fluid mt--7">
    <div class="row">
        <div class="col-xl-12 order-xl-1">
            <div class="card">
                <div class="card-header">
                    <div class="row align-items-center">
                        <div class="col-8">
                            <h3 class="mb-0">{{ __('Intensity Information') }}</h3>
                        </div>
                        <div class="col-4 text-right">
                            <a href="{{ route('intent.index') }}" class="btn btn-sm btn-primary">{{ __('Back to list') }}</a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <form method="post" action="{{ route('intent.store') }}" autocomplete="off">
                        @csrf

                        <h6 class="heading-small text-muted mb-4">{{ __('Intensity Information') }}</h6>
                        <div class="pl-lg-4">
                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-name">{{ __('Name of Area') }}</label>
                                <input type="text" name="name" id="input-name" class="form-control form-control-alternative{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="{{ __('Name') }}" value="{{ old('name') }}" required autofocus>
                                @include('alerts.feedback', ['field' => 'name'])
                            </div>
                            <div class="form-group {{ $errors->has('level') ? 'has-error' : '' }}">
                                <label for="level">Alert Level*</label>
                                <select name="level" id="level" class="form-control select">
                                    @foreach($intensities as $id => $intensities)
                                    <option value="{{ $intensities }}" {{ (in_array($id, old('level', [])) || isset($user) && $user->level->contains($id)) ? 'selected' : '' }}>
                                        {{ $intensities }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group{{ $errors->has('time') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-time">{{ __('Time') }}</label>
                                <input type="time" name="time" id="input-time" class="form-control form-control-alternative{{ $errors->has('time') ? ' is-invalid' : '' }}" placeholder="{{ __('Time') }}" value="{{ old('time') }}" required autofocus>
                                @include('alerts.feedback', ['field' => 'time'])
                            </div>
                            <div class="form-group{{ $errors->has('date') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-date">{{ __('Date') }}</label>
                                <input type="date" name="date" id="input-date" class="form-control form-control-alternative{{ $errors->has('date') ? ' is-invalid' : '' }}" placeholder="{{ __('Date') }}" value="{{ old('date') }}" required autofocus>
                                @include('alerts.feedback', ['field' => 'date'])
                            </div>
                            <div class="form-group{{ $errors->has('latitude') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-latitude">{{ __('Latitude') }}</label>
                                <input type="text" name="latitude" id="input-latitude" class="form-control form-control-alternative{{ $errors->has('latitude') ? ' is-invalid' : '' }}" placeholder="{{ __('Latitude') }}" value="{{ old('latitude') }}" required autofocus>
                                @include('alerts.feedback', ['field' => 'latitude'])
                            </div>
                            <div class="form-group{{ $errors->has('depth') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-depth">{{ __('Depth') }}</label>
                                <input type="text" name="depth" id="input-depth" class="form-control form-control-alternative{{ $errors->has('depth') ? ' is-invalid' : '' }}" placeholder="{{ __('Depth') }}" value="{{ old('depth') }}" required autofocus>
                                @include('alerts.feedback', ['field' => 'depth'])
                            </div>
                            <div class="form-group{{ $errors->has('magnitude') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-magnitude">{{ __('Magnitude') }}</label>
                                <input type="text" name="magnitude" id="input-magnitude" class="form-control form-control-alternative{{ $errors->has('magnitude') ? ' is-invalid' : '' }}" placeholder="{{ __('Magnitude') }}" value="{{ old('magnitude') }}" required autofocus>
                                @include('alerts.feedback', ['field' => 'magnitude'])
                            </div>
                            <div class="form-group{{ $errors->has('epicenter') ? ' has-danger' : '' }}">
                                <label class="form-control-label" for="input-epicenter">{{ __('Epicenter / Location') }}</label>
                                <input type="text" name="epicenter" id="input-epicenter" class="form-control form-control-alternative{{ $errors->has('epicenter') ? ' is-invalid' : '' }}" placeholder="{{ __('Epicenter') }}" value="{{ old('epicenter') }}" required autofocus>
                                @include('alerts.feedback', ['field' => 'epicenter'])
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn btn-success mt-4">{{ __('Save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection