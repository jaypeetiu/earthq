// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here. Other Firebase libraries
// are not available in the service worker.importScripts('https://www.gstatic.com/firebasejs/7.23.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.3.2/firebase-messaging.js');
/*
Initialize the Firebase app in the service worker by passing in the messagingSenderId.
*/
firebase.initializeApp({
    apiKey: 'AIzaSyBEpLJdUfoGlTr_J8ln1FCDIWWm-xKYDpA',
    authDomain: 'earthq-fac16.firebaseapp.com',
    databaseURL: 'earthq-fac16.firebaseapp.com',
    projectId: 'earthq-fac16',
    storageBucket: 'earthq-fac16.appspot.com',
    messagingSenderId: '50160111753',
    appId: '1:50160111753:web:c1eb2f14fdc54b7e99f658',
    measurementId: 'G-L85NGS0Z32',
});

// Retrieve an instance of Firebase Messaging so that it can handle background
// messages.
const messaging = firebase.messaging();
messaging.setBackgroundMessageHandler(function (payload) {
    console.log("Message received.", payload);
    const title = "Hello world is awesome";
    const options = {
        body: "Your notificaiton message .",
        icon: "/firebase-logo.png",
    };
    return self.registration.showNotification(
        title,
        options,
    );
});