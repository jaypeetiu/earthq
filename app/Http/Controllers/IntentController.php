<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreIntentRequest;
use App\Intent;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use ExpoSDK\Expo;
use ExpoSDK\ExpoMessage;

class IntentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Intent $model)
    {
        abort_unless(Gate::allows('admin_access'), 403);
        return view('intent.index', ['intents' => $model->orderBy('id', 'desc')->paginate(15)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        abort_unless(Gate::allows('admin_access'), 403);

        $intensities = ["2", "3", "4", "5", "6", "7", "8"];

        return view('intent.create', compact('intensities'));
    }

    public function sendAlert($id)
    {
        abort_unless(Gate::allows('admin_create'), 403);
        $users_tokens = User::all('device_key')
            ->where('device_key', '!=', null)
            ->where('device_key', '!=', 'false')
            ->toArray();

        //Send Push Notification

        $SERVER_API_KEY = 'AAAAC63GkIk:APA91bGYUaW9ewV044bLyciTr9-uvojJ2lCU8eYR3XmoPy9ZNEnpZOC-NZDTdWSgrSIkcYYkpLy7Xo5mkg1Bh-787BJej3b1ti-8xDb5TFjuZqBLuAYZI9iDdfkU9aoTRkPbzq_0TVDF';


        foreach ($users_tokens as $key => $value) {
            foreach ($value as $k => $val) {
                $data = [
                    "registration_ids" => [$val], // after registering on mobile automatic save device token
                    "notification" => [
                        "title" => "ShakeCDO Alert Message",
                        "body" => 'Intensity Level: ' . $id,
                        "sound" => 'alarm.mp3',
                        "color" => 'red',
                        "android_channel_id" => 'default',
                    ],
                    "data" => [
                        "url" => "https://earthq.page.link/home"
                    ],
                ];
                $dataString = json_encode($data);

                $headers = [
                    'Authorization: key=' . $SERVER_API_KEY,
                    'Content-Type: application/json',
                ];

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

                $response = curl_exec($ch);
            }
        }


        return back()->withStatus(__('Intensity ' . $id . ' Alert! '));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        abort_unless(Gate::allows('admin_create'), 403);
        $users_tokens = User::all('device_key')
            ->where('device_key', '!=', null)
            ->where('device_key', '!=', 'false')
            ->toArray();

        $SERVER_API_KEY = 'AAAAC63GkIk:APA91bGYUaW9ewV044bLyciTr9-uvojJ2lCU8eYR3XmoPy9ZNEnpZOC-NZDTdWSgrSIkcYYkpLy7Xo5mkg1Bh-787BJej3b1ti-8xDb5TFjuZqBLuAYZI9iDdfkU9aoTRkPbzq_0TVDF';

        $int = Intent::create($request->all());
        $int->save();

        foreach ($users_tokens as $key => $value) {
            foreach ($value as $k => $val) {
                $data = [
                    "registration_ids" => [$val], // after registering on mobile automatic save device token
                    "notification" => [
                        "title" => "Area: " . $request->name,
                        "body" => 'Intensity Level: ' . $request->level . ' Time: ' . $request->time . ' Date: ' . $request->date . ' Latitude: ' . $request->latitude . ' Depth: ' . $request->depth . ' Magnitude: ' . $request->magnitude . ' Epicenter: ' . $request->epicenter,
                        "sound" => 'toot.mp3',
                        "color" => 'red',
                        "android_channel_id" => 'default',
                    ],
                ];
                $dataString = json_encode($data);

                $headers = [
                    'Authorization: key=' . $SERVER_API_KEY,
                    'Content-Type: application/json',
                ];

                $ch = curl_init();

                curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

                $response = curl_exec($ch);
            }
        }

        return redirect()->route('intent.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Intent $intent)
    {
        abort_unless(Gate::allows('admin_access'), 403);
        $intensities = ["2", "3", "4", "5", "6", "7", "8"];

        return view('intent.edit', compact('intent', 'intensities'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Intent $intent)
    {
        abort_unless(Gate::allows('admin_access'), 403);

        $intent->update($request->all());

        return redirect()->route('intent.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Intent::where('id', $id)->delete();

        return back();
    }
}
