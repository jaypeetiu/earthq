<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Intent;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\View\View
     */
    public function index(Intent $model)
    {
        if(Gate::allows('admin_access')){

            $user = Auth::user();
            return view('dashboard', compact('user'));
        }else{
            $user = Auth::user();
            $intents = $model->orderBy('id', 'desc')->paginate(5);
            return view('userdashboard', compact('user','intents'));
        }
        
    }
}
