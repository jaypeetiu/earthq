<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * Display a listing of the users
     *
     * @param  \App\User  $model
     * @return \Illuminate\View\View
     */
    public function index(User $model)
    {
        abort_unless(Gate::allows('admin_access'), 403);
        return view('users.index', ['users' => $model->paginate(15)]);
    }

    /**
     * Show the form for creating a new user
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        abort_unless(Gate::allows('user_access'), 403);
        return view('users.create');
    }

    /**
     * Store a newly created user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $model
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(UserRequest $request, User $model)
    {
        abort_unless(Gate::allows('user_access'), 403);
        $model->create($request->merge(['password' => Hash::make($request->get('password'))])->all());

        return redirect()->route('user.index')->withStatus(__('User successfully created.'));
    }

    /**
     * Show the form for editing the specified user
     *
     * @param  \App\User  $user
     * @return \Illuminate\View\View
     */
    public function edit(User $user)
    {
        abort_unless(Gate::allows('user_access'), 403);
        if ($user->id == 1) {
            return redirect()->route('user.index');
        }

        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified user in storage
     *
     * @param  \App\Http\Requests\UserRequest  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UserRequest $request, User  $user)
    {
        abort_unless(Gate::allows('user_access'), 403);
        $hasPassword = $request->get('password');
        $user->update(
            $request->merge(['password' => Hash::make($request->get('password'))])
                ->except(
                    [$hasPassword ? '' : 'password']
                )
        );

        return redirect()->route('user.index')->withStatus(__('User successfully updated.'));
    }

    /**
     * Remove the specified user from storage
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(User  $user)
    {
        abort_unless(Gate::allows('user_access'), 403);
        if ($user->id == 1) {
            return abort(403);
        }

        $user->delete();

        return redirect()->route('user.index')->withStatus(__('User successfully deleted.'));
    }


    public function sendNotificationToUser($id)
    {

        //Send Push Notification

        $SERVER_API_KEY = 'AAAAC63GkIk:APA91bGYUaW9ewV044bLyciTr9-uvojJ2lCU8eYR3XmoPy9ZNEnpZOC-NZDTdWSgrSIkcYYkpLy7Xo5mkg1Bh-787BJej3b1ti-8xDb5TFjuZqBLuAYZI9iDdfkU9aoTRkPbzq_0TVDF';

        $data = [
            "registration_ids" => 'device_tokens', // after registering on mobile automatic save device token
            "notification" => [
                "title" => "You have a new chat message",
                "body" => " ",
            ]
        ];
        $dataString = json_encode($data);

        $headers = [
            'Authorization: key=' . $SERVER_API_KEY,
            'Content-Type: application/json',
        ];

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);

        $response = curl_exec($ch);
    }

    public function storeToken(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        try {
            if ($user->device_key != $request->token) {
                $user->device_key = $request->token;
                $user->save();
            }
            // echo $devicetoken;
            return response()->json([
                'user' => $request->id,
                'message' => $request->token,
            ]);
        } catch (\Throwable $th) {
            //throw $th;
        }

        // try {
        //     $request->validate([
        //         'device_token' => 'required|string',
        //     ]);
        //     //Saves Device Token to DB
        //     $user = User::where('id', auth()->user()->id)->first();
        //     if ($user->device_key == null) {
        //         $user->device_key = $request->device_token;
        //         $user->save();
        //     }
        //     else if($user->device_key !== $request->expoPushToken){
        //         $user->device_key = $request->expoPushToken;
        //         $user->update();
        //     }
        //     return response($user);
        // }
        // catch(\Exception $e) {
        //     return response($e);
        // }

        // return response()->json([
        //     'message' => 'test'
        // ]);
    }
}
