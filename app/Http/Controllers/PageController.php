<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Gate;

class PageController extends Controller
{
    /**
     * Display icons page
     *
     * @return \Illuminate\View\View
     */
    public function icons()
    {
        abort_unless(Gate::allows('user_access'), 403);
        return view('pages.icons');
    }

    /**
     * Display maps page
     *
     * @return \Illuminate\View\View
     */
    public function maps()
    {
        abort_unless(Gate::allows('user_access'), 403);
        return view('pages.maps');
    }

    /**
     * Display tables page
     *
     * @return \Illuminate\View\View
     */
    public function tables()
    {
        abort_unless(Gate::allows('user_access'), 403);
        return view('pages.tables');
    }

    /**
     * Display notifications page
     *
     * @return \Illuminate\View\View
     */
    public function notifications()
    {
        abort_unless(Gate::allows('user_access'), 403);
        return view('pages.notifications');
    }

    /**
     * Display rtl page
     *
     * @return \Illuminate\View\View
     */
    public function rtl()
    {
        abort_unless(Gate::allows('user_access'), 403);
        return view('pages.rtl');
    }

    /**
     * Display typography page
     *
     * @return \Illuminate\View\View
     */
    public function typography()
    {
        abort_unless(Gate::allows('user_access'), 403);
        return view('pages.typography');
    }

    /**
     * Display upgrade page
     *
     * @return \Illuminate\View\View
     */
    public function upgrade()
    {
        abort_unless(Gate::allows('user_access'), 403);
        return view('pages.upgrade');
    }
}
